for (var i = 0; i < 64; i++) {

    var square = document.createElement("div")
    square.setAttribute("style", `display: inline-block; height: 12.5%; width: 12.5%; background-color: ${(Math.floor(i / 8 ) % 2) == ((i % 8) % 2)  ? "#a6825b": "#ffe4c7"};`)
    square.setAttribute("id", i)
    

    square.addEventListener("click", function() {

        clicked_square = parseInt(this.getAttribute("id"));
        
        if(is_enabled(clicked_square)) {

            if(!is_queen_on_square(clicked_square)) {

                var queen_img = document.createElement("img")
                queen_img.setAttribute("style", `display: inline-block; height: 100%; width: 100%;`)
                queen_img.setAttribute("src", `https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/Chess_qdt45.svg/1024px-Chess_qdt45.svg.png`)
                this.appendChild(queen_img)
    
                disable_row(clicked_square)
                disable_col(clicked_square)
                disable_diagonals(clicked_square)
            }
            else {
                this.removeChild(this.childNodes[0])
                enable_row(clicked_square)
                enable_col(clicked_square)
                enable_diagonals(clicked_square)
            }
        }
    })
    document.getElementById("board").appendChild(square)
}







document.getElementById("btn-find-next").addEventListener("click", function() {
    run_algorithm(true)
})

document.getElementById("btn-find-all").addEventListener("click", function() {
    run_algorithm(false)
})



async function run_algorithm(find_only_next_solution) {

    var start_square = 0
    var last_placed_queen_square = 0
    var nr_of_placed_queens = 0

    if(continuing_from_solved_board()) {
        nr_of_placed_queens = 8
        remove_the_last_two_placed_queens()
    }

    while(true) {
        place_queens_from_starting_square_until_no_more_available_squares()

        if(no_more_available_squares_after_removing_last_placed_queen()) {
            remove_the_second_last_placed_queen()
        }
        else {
            if(nr_of_placed_queens == 7) {
                console.log("7 queens placed")
            }
            if(nr_of_placed_queens == 8) {
                console.log("8 queens placed")
                update_nr_of_found_solutions_counter();
                
                if(find_only_next_solution || nr_of_found_solutions() == 92) {
                    break
                }
                else {
                    // Show each solved board if find all is clicked
                    await updateUIByPausing(); 
                }
            }
            remove_last_placed_queen_and_set_starting_square_to_the_adjacent_square()

            // Show each loop iteration if find next is clicked
            if(find_only_next_solution) {
                await updateUIByPausing() 
            }
        }
    }







    function continuing_from_solved_board() {
        return document.getElementById("nr").innerHTML > 0;
    }

    function place_queens_from_starting_square_until_no_more_available_squares() {
        for(var i = start_square; i < 64; i++) {
            if(is_enabled(i) && !is_queen_on_square(i)) {
                document.getElementById(i).click();
                nr_of_placed_queens++
                last_placed_queen_square = i;
            }
        }
    }

    function remove_the_last_two_placed_queens() {
        for (var i = 63; i >= 0; i--) {
            if(is_queen_on_square(i)) {
                document.getElementById(i).click();
                last_placed_queen_square = i
                start_square = i + 1
                nr_of_placed_queens--
                i = -1
            }
        }
        remove_the_second_last_placed_queen()
    }

    function remove_the_second_last_placed_queen() {
        for (var i = last_placed_queen_square - 1; i >= 0; i--) {
            if(is_queen_on_square(i)) {
                document.getElementById(i).click();
                start_square = i + 1;
                nr_of_placed_queens--
                i = -1
            }
        }
    }

    function no_more_available_squares_after_removing_last_placed_queen() {
        return last_placed_queen_square == start_square -1
    }

    function remove_last_placed_queen_and_set_starting_square_to_the_adjacent_square() {
        document.getElementById(last_placed_queen_square).click();
        nr_of_placed_queens--
        start_square = last_placed_queen_square + 1;
    }

    async function updateUIByPausing() {
        await new Promise(resolve => setTimeout(resolve, 1))
    }

    function update_nr_of_found_solutions_counter() {
        var nr_of_solutions_found = document.getElementById("nr").innerHTML;
        nr_of_solutions_found++
        document.getElementById("nr").innerHTML = nr_of_solutions_found.toString();
    }

    function nr_of_found_solutions() {
        return document.getElementById("nr").innerHTML 
    }
}






function disable_row(clicked_square) {
    var start_square = clicked_square - (clicked_square % 8)
    var end_square = start_square + 8
    for (var i = start_square; i < end_square; i++) {
        disable_square(i)
    }
}

function disable_col(clicked_square) {
    var start_square = clicked_square % 8
    for (var i = start_square; i < 64; i+=8) {
        disable_square(i)
    }
}

function disable_diagonals(clicked_square) {
    var start_square = clicked_square
    for (var i = start_square; i < 64; i+=9) {
        disable_square(i)
        if(i % 8 == 7) {
            break;
        }
    }

    var start_square = clicked_square
    for (var i = start_square; i >= 0; i-=9) {
        disable_square(i)
        if(i % 8 == 0) {
            break;
        }
    }

    var start_square = clicked_square
    for (var i = start_square; i < 64; i+=7) {
        disable_square(i)
        if(i % 8 == 0) {
            break;
        }
    }

    var start_square = clicked_square
    for (var i = start_square; i >= 0; i-=7) {
        disable_square(i)
        if(i % 8 == 7   ) {
            break;
        }
    }
}







function enable_row(clicked_square) {
    var start_square = clicked_square - (clicked_square % 8)
    var end_square = start_square + 8
    for (var i = start_square; i < end_square; i++) {
        if(!(is_queen_on_same_row_as_square(i) || is_queen_on_same_col_as_square(i) || is_queen_on_same_diagonals_as_square(i))) {
            enable_square(i)
        }
    }
}

function enable_col(clicked_square) {
    var start_square = clicked_square % 8
    for (var i = start_square; i < 64; i+=8) {
        if(!(is_queen_on_same_row_as_square(i) || is_queen_on_same_col_as_square(i) || is_queen_on_same_diagonals_as_square(i))) {
            enable_square(i)
        }
    }
}

function enable_diagonals(clicked_square) {
    var start_square = clicked_square
    for (var i = start_square; i < 64; i+=9) {
        if(!(is_queen_on_same_row_as_square(i) || is_queen_on_same_col_as_square(i) || is_queen_on_same_diagonals_as_square(i))) {
            enable_square(i)
        }
        if(i % 8 == 7) {
            break;
        }
    }

    var start_square = clicked_square
    for (var i = start_square; i >= 0; i-=9) {
        if(!(is_queen_on_same_row_as_square(i) || is_queen_on_same_col_as_square(i) || is_queen_on_same_diagonals_as_square(i))) {
            enable_square(i)
        }
        if(i % 8 == 0) {
            break;
        }
    }

    var start_square = clicked_square
    for (var i = start_square; i < 64; i+=7) {
        if(!(is_queen_on_same_row_as_square(i) || is_queen_on_same_col_as_square(i) || is_queen_on_same_diagonals_as_square(i))) {
            enable_square(i)
        }
        if(i % 8 == 0) {
            break;
        }
    }

    var start_square = clicked_square
    for (var i = start_square; i >= 0; i-=7) {
        if(!(is_queen_on_same_row_as_square(i) || is_queen_on_same_col_as_square(i) || is_queen_on_same_diagonals_as_square(i))) {
            enable_square(i)
        }
        if(i % 8 == 7   ) {
            break;
        }
    }
}







function is_queen_on_same_row_as_square(clicked_square) {
    var start_square = clicked_square - (clicked_square % 8)
    var end_square = start_square + 8
    for (var i = start_square; i < end_square; i++) {
        if(is_queen_on_square(i)) {
            return true
        }
    }
    return false
}

function is_queen_on_same_col_as_square(clicked_square) {
    var start_square = clicked_square % 8
    for (var i = start_square; i < 64; i+=8) {
        if(is_queen_on_square(i)) {
            return true
        }
    }
    return false
}

function is_queen_on_same_diagonals_as_square(clicked_square) {
    var start_square = clicked_square
    for (var i = start_square; i < 64; i+=9) {
        if(is_queen_on_square(i)) {
            return true
        }
        if(i % 8 == 7) {
            break;
        }
    }

    var start_square = clicked_square
    for (var i = start_square; i >= 0; i-=9) {
        if(is_queen_on_square(i)) {
            return true
        }
        if(i % 8 == 0) {
            break;
        }
    }

    var start_square = clicked_square
    for (var i = start_square; i < 64; i+=7) {
        if(is_queen_on_square(i)) {
            return true
        }
        if(i % 8 == 0) {
            break;
        }
    }

    var start_square = clicked_square
    for (var i = start_square; i >= 0; i-=7) {
        if(is_queen_on_square(i)) {
            return true
        }
        if(i % 8 == 7   ) {
            break;
        }
    }
    return false
}







function enable_square(id) {
    document.getElementById(id).setAttribute("style", `display: inline-block; height: 12.5%; width: 12.5%; background-color: ${(Math.floor(id / 8 ) % 2) == ((id % 8) % 2)  ? "#a6825b": "#ffe4c7"};`)
    
    if(document.getElementById(id).childNodes.length == 1) {
        document.getElementById(id).removeChild(document.getElementById(id).childNodes[0])
    }
}

function disable_square(id) {
    document.getElementById(id).setAttribute("style", `display: inline-block; height: 12.5%; width: 12.5%; background-color: ${(Math.floor(id / 8 ) % 2) == ((id % 8) % 2)  ? "grey": "darkgrey"};`)

    if(document.getElementById(id).childNodes.length == 0) {
        var queen_img = document.createElement("img")
        queen_img.setAttribute("style", `display: inline-block; height: 100%; width: 100%;`)
        queen_img.setAttribute("src", `https://cdn1.iconfinder.com/data/icons/internet-28/48/51-512.png`)
        document.getElementById(id).appendChild(queen_img)
    }
}

function is_queen_on_square(id) {
    return document.getElementById(id).childNodes[0] && document.getElementById(id).childNodes[0].src == `https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/Chess_qdt45.svg/1024px-Chess_qdt45.svg.png`
}

function is_enabled(id) {
    return document.getElementById(id).childNodes[0] == undefined || document.getElementById(id).childNodes[0].src != `https://cdn1.iconfinder.com/data/icons/internet-28/48/51-512.png`
}




